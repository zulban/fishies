﻿using UnityEngine;
using System.Collections;

public class ClickTool : MonoBehaviour
{
    private float range = 50;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject clickedObject = GetClickedObject();
        }
    }

    private GameObject GetClickedObject()
    {
        Vector3 pos = transform.position;
        Vector3 forward = Camera.main.transform.forward;
        RaycastHit rayCastHit = new RaycastHit();
        bool isHit = Physics.Linecast(pos, pos + forward * range, out rayCastHit);
        if (!isHit)
            return null;
        return rayCastHit.collider.gameObject;
    }

}
